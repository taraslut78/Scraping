from bs4 import BeautifulSoup as bs
import os
from selenium import webdriver
import re
from sys import argv
from platform import system


def get_page(url="https://www.herold.at/gelbe-seiten/was_", was=''):
    curent_system = system()
    try:
        if str(curent_system).lower() == 'linux':
            driver = webdriver.Firefox(executable_path=os.path.dirname(os.path.abspath(__file__)) + '/geckodriver_linux')
        if str(curent_system).lower() == 'windows':
            driver = webdriver.Firefox(
                executable_path=os.path.dirname(os.path.abspath(__file__)) + '/geckodriver.exe')
    except:
        driver.close()
        exit(1)

    pages = []
    while True:
        try:
            url += was
            html = driver.get(url)
            html = driver.page_source
            bsObj = bs(html, "html.parser")
        except:
            driver.close()
            exit(1)

        pages.append(bsObj)

        next_link = bsObj.find("ul", {"class": "pagination"})
        if next_link is not None:
            next_link = next_link.find('li', {'class': "page-item-last"})
            if next_link is not None:
                next_link = next_link.find("a").attrs['href']
                if next_link is not None:
                    url = next_link
                    was = ''
                else:
                    break
            else:
                break
        else:
            break
    driver.close()
    return pages


def page_to_cards(pages):
    i= 1
    for bsObj in pages:
        cards = bsObj.find_all("div", {"class": "result-item"})
        for item in cards:
            print("#", i)
            name = item.find_all("span", {"itemprop": "name"})[0].text
            print(name)
            address = item.find_all('p', {"class": "address"})[0].text
            print(address)
            phone = item.find_all('div', {"class": "dropdown"})[0].text[13:]
            print(phone)
            detail_url = item.find_all("div", {"class": "result-item-details"})[0].find("a").attrs['href']
            print(detail_url)
            i += 1

if __name__ == "__main__":

    if len(argv) < 2:
        print("""
        Usage of program is 
        python Proba_for_thomas.py keywords
        """)
        exit(0)
    was = " ".join(argv[1:])
    print(was)
    pages = get_page(was=was)
    page_to_cards(pages)

        # print(item, "\n")

